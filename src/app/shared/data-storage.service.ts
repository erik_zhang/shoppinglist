import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {RecipeService} from '../recipes/recipe.service';

@Injectable({providedIn: 'root'})
export class DataStorageService {
  private firebaseUrl = 'https://ng-sandbox-dummy.firebaseio.com/';

  constructor(private http: HttpClient, private recipeService: RecipeService) {

  }
  storeRecipes() {
    const recipes = this.recipeService.getRecipes();
    this.http.put(this.firebaseUrl + '/recipes.json',
      recipes
    ).subscribe(
      response => {
        console.log(response);
      }
    );
  }

}
