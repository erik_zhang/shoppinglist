import {Recipe} from './recipe.model';
import {Injectable} from '@angular/core';
import {Ingredient} from '../shared/ingredient.model';
import {ShoppingListService} from '../shopping-list/shopping-list.service';
import {Subject} from 'rxjs';

@Injectable({providedIn: 'root'})
export class RecipeService {
  recipesChanged = new Subject<Recipe[]>();


  private recipes: Recipe[] = [
    new Recipe('Blueberry pie', 'blueberry',
      'https://cdn.pixabay.com/photo/2014/04/02/11/03/pie-305367_960_720.png',
      [new Ingredient('Blueberry', 2000),
      new Ingredient('Crust', 1400)]),
    new Recipe('Blue Raspberry pie', 'not blueberry',
      'https://cdn.pixabay.com/photo/2014/04/02/11/03/pie-305367_960_720.png',
      [new Ingredient('Raspberry', 3000),
        new Ingredient('Crust', 400)])
  ];


  constructor(private slService: ShoppingListService) {

  }

  public getRecipes() {
    return this.recipes.slice();
  }

  public getRecipe(id: number) {
    return this.recipes[id];
  }


  public addIngredientsToShoppingList(ingredients: Ingredient[]) {
    this.slService.addIngredients(ingredients);
  }

  addRecipe(recipe: Recipe) {
    this.recipes.push(recipe);
    this.recipesChanged.next(this.recipes.slice());
  }


  updateRecipe(index: number, newRecipe: Recipe) {
    this.recipes[index] = newRecipe;
    this.recipesChanged.next(this.recipes.slice());
  }

  deleteRecipe(index: number){
    this.recipes.splice(index, 1);
    this.recipesChanged.next(this.recipes.slice());
  }
}
